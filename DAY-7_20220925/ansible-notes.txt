Ansible Use Case :

STEP-1 : Launched 2 Linux Machines on AWS Cloud 

STEP-2 : Download Install & Configure Ansible on 1 Linux Machine (Ansible Controller)

To install pip, in case it’s not already available on your system:

$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
$ python get-pip.py --user

After pip is installed:
$ python -m pip install --user ansible

You can test on your terminal if it’s successfully installed by running:
$ ansible --version


Install Ansible on Ubuntu :

$ sudo apt-get install software-properties-common -y 
$ sudo add-apt-repository --yes --update ppa:ansible/ansible
$ sudo apt-get install ansible -y 


STEP-3 : Create SSH Keys on Controller Machine and Share with Node-1 

# ssh-keygen 

STEP-4 : Test the connection from Ansible Controller To Node-1 

$ ssh node-1_ipaddress

STEP-5 : Execute Ansible Ad-hoc Commands on Ansible Controller 

$ ansible -m ping node-1
$ ansible node-1 -m copy -a "src=/etc/hosts dest=/tmp/hosts"
$ ansible node-1 -m file -a "dest=/srv/foo/a.txt mode=600"

STEP-6 : Write Ansible Playbook and Execute on Ansible Controller and Node-1 

Ansible Basic Concepts and Terms :

1. Host: A remote machine managed by Ansible.
2. Group: Several hosts grouped together that share a common attribute.
3. Inventory: A collection of all the hosts and groups that Ansible manages. Could be a static file in the simple cases or we can pull the inventory from remote sources, such as cloud providers.
4. Modules: Units of code that Ansible sends to the remote nodes for execution.
5. Tasks: Units of action that combine a module and its arguments along with some other parameters.
​​6. Playbooks: An ordered list of tasks along with its necessary parameters that define a recipe to configure a system.
7. Roles: Redistributable units of organization that allow users to share automation code easier. 
8. YAML: A popular and simple data format that is very clean and understandable by humans.

---
- name: Copy a File From Ansible Controller To Ansible Node-1
  hosts: node-1
  become: yes
  tasks:
  - name: Copy a File 
    ansible.builtin.copy:
      src: ./devopstools.txt
      dest: /tmp/devopstools.txt
      mode: '0644'

...

ubuntu@ansible-controller:~$ cat /etc/ansible/hosts
# Adding Ansible Nodes Details

[web]
172.31.81.53
#172.31.81.54
#172.31.81.55
#172.31.81.56
#172.31.81.57

#[app]
#172.31.81.60
#172.31.81.65
#172.31.81.70

Syntax Check:
$ ansible-playbook --syntax intro_playbook.yml

Preview/Dry Run the Playbook:
$ ansible-playbook --check intro_playbook.yml

Execute the Playbook:
$ ansible-playbook intro_playbook.yml

STEP-7 : Create Ansible Roles and Deploy unto Node-1 

STEP-8 : Validate 

STEP-9 : CleanUp 

https://docs.ansible.com/ansible/2.5/user_guide/intro_adhoc.html
https://docs.ansible.com/ansible/2.5/cli/ansible-playbook.html?highlight=ansible%20playbooks
https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html
https://galaxy.ansible.com/
