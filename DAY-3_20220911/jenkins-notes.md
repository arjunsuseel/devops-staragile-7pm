# We are setting up SelfHosted Jenkins on Linux(Ubuntu 22.04)


# STEP-1 : Go to Cloud Vendors i.e. AWS or Azure 

# Compute --> EC2 --> Instances 
    -> Linux(Ubuntu 22.04)
        -> Java 11:
            - Configure Env
        -> Maven :
            - Configure Env

        -> Download, Install & Configure Jenkins 
        -> Unlock the Jenkins
        -> Install Suggested plugins [Community]
        -> Configure a User and Login to Jenkins
        -> Configure Java at Jenkins UI level
        -> Configure maven at Jenkins UI level
        -> Install Maven plugin

        -> Create Jenkins Job :
            - VCS/SCM : GitHub --> Repository --> develop
        -> Pipelines :

AWS CodeBuild & CodePipeline

IaaS
PaaS
DevOps :
    - SaaS :
        - AWS CodeCommit
        - AWS CodeBuild
        - AWS CodeArtifact
        - AWS CodeDeploy
        - AWS CodePipeline

https://www.jenkins.io/
https://pkg.jenkins.io/debian-stable/
https://www.jenkins.io/doc/
https://www.jenkins.io/doc/book/installing/
https://www.jenkins.io/doc/book/installing/linux/


https://docs.aws.amazon.com/codeartifact/latest/ug/using-maven-packages-in-codebuild.html
https://docs.aws.amazon.com/codeartifact/latest/ug/maven-mvn.html


Q?


Add this distribution management configuration to your pom.xml

<distributionManagement>
  <repository>
    <id>cloudbinary-cloudbinary</id>
    <name>cloudbinary-cloudbinary</name>
    <url>https://cloudbinary-266934144328.d.codeartifact.us-east-1.amazonaws.com/maven/cloudbinary/</url>
  </repository>
</distributionManagement>

 
 Export a CodeArtifact authorization token for authorization to your repository from your preferred shell (token expires in 12 hours).

 export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain cloudbinary --domain-owner 266934144328 --query authorizationToken --output text`


 Add a profile containing your repository to your settings.xml.

 <profiles>
  <profile>
    <id>cloudbinary-cloudbinary</id>
    <activation>
      <activeByDefault>true</activeByDefault>
    </activation>
    <repositories>
      <repository>
        <id>cloudbinary-cloudbinary</id>
        <url>https://cloudbinary-266934144328.d.codeartifact.us-east-1.amazonaws.com/maven/cloudbinary/</url>
      </repository>
    </repositories>
  </profile>
</profiles>


Add your server to the list of servers to your settings.xml.

<servers>
  <server>
    <id>cloudbinary-cloudbinary</id>
    <username>aws</username>
    <password>${env.CODEARTIFACT_AUTH_TOKEN}</password>
  </server>
</servers>

(Optional) Set a mirror in your settings.xml that captures all connections and routes them to your repository instead of a public repository.

<mirrors>
  <mirror>
    <id>cloudbinary-cloudbinary</id>
    <name>cloudbinary-cloudbinary</name>
    <url>https://cloudbinary-266934144328.d.codeartifact.us-east-1.amazonaws.com/maven/cloudbinary/</url>
    <mirrorOf>*</mirrorOf>
  </mirror>
</mirrors>



