Agenda :

Module 2 - Managing Source Code - Git and GitHub [Completed]
Topics:
•	Overview of Version Control systems
•	Central vs Distributed Control systems
•	Introduction to Git
•	Git file workflow
•	Important Git Commands
•	Branching and Merging, Stashing, Rebasing, Reverting and Resetting 
•	Introduction to GitHub
•	Using Git and GitHub together

Rebasing [Pending]

Module 3 – Understanding and using Build tools [Completed]
Topics:
•	Overview of Various Build tools
•	What is Maven
•	Maven Plugins
•	Maven Archetypes
•	Project Object Model (POM)
•	Source Control Integration

Module 5 - Continuous Integration using Jenkins [Completed]
Topics: 
•	Overview of Continuous Integration
•	Overview of Jenkins 
•	Jenkins architecture
•	Installing and Configuring Jenkins
•	Jenkins Management
•	Jenkins Build Pipeline

Practical’s to be covered: 
•	Installing and configuring Jenkins
•	Creating a build using Jenkins
•	Integrating with Jenkins
•	Working with Jenkins Pipelines

Q?

Python - Django : 
    - pip --> requirements.txt [django, pysyc]

Java - SpringBoot :
    - maven | ant | gradle ---> pom.xml   |  java & maven & IDE tool :  Eclipse 

Jenkins --> Continuous Integration | Run Build Jobs, Release jobs, infra jobs etc..
Plugin Manager : create a job 
    -> java
    -> maven 

Java Source Code :
https://github.com/kesavkummari/awsdevops-staragile-java.git
