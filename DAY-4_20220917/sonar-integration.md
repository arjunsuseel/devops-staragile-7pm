Configure the SONAR_TOKEN environment variable
Name of the environment variable: SONAR_TOKEN 
Value of the environment variable: ac36641c868673e6ab80d0d10c78ee335b8e58ad 
Execute the SonarScanner for Maven from your computer
Update your pom.xml file with the following properties:
<properties>
  <sonar.organization>opswork</sonar.organization>
  <sonar.host.url>https://sonarcloud.io</sonar.host.url>
</properties>
Run the following command in the project folder:

mvn verify org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.projectKey=opsworkjava
If you wish, you can shorten this command (to mvn verify sonar:sonar, for example) by specifying a prefix for the plugin.

