WebServers : Proxy 
    - Static Code / Business Logic 

Application Servers:
    - Dynamic Code / Business Logic 
    1. IBM : WebSphere
    2. Oracle : WebLogic
    3. GNU : GlassFish
    4. Apache : Tomcat
    5. GNU : Wildfly
    6. Redhat : Jboss 

User --> https://cloudbinary.io --> Global DNS --> NS --> AWS --> ELB --> Targets(Web EC2 instance --> App EC2 Instance --> DB EC2/RDS instance) : 

Application Servers :
    - Apache : Tomcat( Deploy Application Code i.e. cloudbinary-5.0.0.war )
    - Cloud Computing : AWS 
        - Compute :
            - EC2 :
                - Instances :
                    - Windows 
                    - Linux (Ubuntu 22.04)

A Traditional System Admin Job Roles & Responsibilities :

1. Download, Install & Configure 
2. Backup & Recovery  
3. User Management
4. Permissions
5. Monitoring 
6. Setting up servers 
7. Networks  i.e. IPV4 and IPV6 

    - Windows
    - Linux :
        System User Commands : root --> shutdown
            /usr/sbin/
        
        Normal User Commands : ubuntu 
            /usr/bin/ 

        - Basic Commands  :
            # whatis pwd
            # whereis pwd
            # man pwd 
        - Advanced Commands : 

        - Scripts 
        - Package Management :
        - debian / ubuntu  : https://ubuntu.com/server/docs/package-management
            # apt update            or apt-get update
            # apt install tree -y   or apt-get install tree -y 

        CentOs/Redhat/Amazonlinux : https://www.redhat.com/sysadmin/how-manage-packages
            # yum install tree -y 

        - Controlling  Services & Deamons 
        - Log Management
        - Process Management
        - User & Group Management
        - Permissions
        - Text Editors : vi or vim 
        - Web Servers :
            - IIS 
            - Apache2 - Ubuntu
            - Apache httpd - CentOS/Redhat etc.. 
            - Nginx for all Linux/Unix distributions 
        - Databases : Oracle MySQL  
        - Application Servers : Apache Tomcat
        - Continuous Integration Tool : Jenkins 
        - Continuous Static Code Analysis Tool : Sonarqube
        - Continuous Binary Code Repository Tool : Jfrog 


# Download, Install & Configure a Software in linux/unix/windows/macos :
    - Two Types :
        
        - Package Manager
            # apt update 
            # apt install tree -y
                # Configure 
                # Start the service

        - Binary 
            # Download a zip file 
            # unizp 
            # go inside and configure
            # start the service

# Download, Install & Configure Apache Tomcat in linux:
    - Package Manager :

    - Binary :
    
    # wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.82/bin/apache-tomcat-8.5.82.tar.gz

    # tar xzvf apache-tomcat-8.5.82.tar.gz

    # ls -lrt apache-tomcat-8.5.82

    # mv apache-tomcat-8.5.82 tomcat 

    # cd tomcat 

    # Tomcat Default Port :
        - 8080 
        
    # Enable Tomcat Services :
    Restricted Globally :
        - Server Status 
        - Manager App
        - Host Manager 

    User Management :
    # UserName : admin
    # PassWord : redhat@123

    Permissions To Access Tomcat Funcationality:
    # Roles :
        - Manager 
        - Admin 

    Note: Assign the Roles to User, so that user can access tomcat Funcationality