# Jenkins Pipeline :

AWS CodePipeline:
    - AWS CodeBuild (Source Code GitHub)
    - AWS CodeBuild (Compile the Source Code)
    - AWS CodeBuild (CodeScanning Using SonarQube)
    - AWS CodeBuild (Publish Artifacts using s3 / aws codeartifact / Jfrog / Nexus)

Jenkins : Self-Hosted Continuous Integration Server :
    - Jenkins Jobs: 
        - Source Code GitHub
        - Compile the Source Code
        - CodeScanning Using SonarQube
        - Publish Artifacts using s3 / aws codeartifact / Jfrog / Nexus
    - Pipeline :
        - Jenkinsfile 
A Jenkinsfile can be written using two types of syntax - Declarative and Scripted.
            - Declarative :
                - Declarative Pipelines are break down stages into individual stages that can contain multiple steps.
                - Use a Keyword called pipeline in the start of the file

            - Scripted :
                - Scripted pipelines use Groovy code and references to the Jenkins Pipeline DSL within the stage elements without the need for steps.
                - Use a Keyword called node in the start of the file
